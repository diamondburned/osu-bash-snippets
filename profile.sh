#!/bin/bash
# Usage: ./lookup [osu|ctb|mania|taiko] [API] [osu! username] 

if [[ $1 = "mania" ]]; then
	gamemode="3"
	gametype="osu! Mania"
elif [[ $1 = "taiko" ]]; then
	gamemode="1"
	gametype="osu! Taiko"
elif [[ $1 = "ctb" ]]; then
	gamemode="2"
	gametype="osu! Catch the Beat"
else
	gamemode="0"
	gametype="osu!"
fi
input="${@:3}"
API="$2" # Your API here

CURL=$(curl -s osu.ppy.sh/api/get_user -d "k=$API" -d "u=$input" -d "m=$gamemode")
if [[ $CURL = "[]" ]]; then # invalid username
	echo Invalid username!
	exit
else # valid username
	username="$input"
fi

CURLusername=$(echo "$CURL" |		grep -F '[0,"username"]' |			awk -F '	' '{print $2}' | cut -d '"' -f 2)
CURLuser_id=$(echo "$CURL" |		grep -F '[0,"user_id"]' |			awk -F '	' '{print $2}' | cut -d '"' -f 2)
CURLcountry=$(echo "$CURL" |		grep -F '[0,"country"]' |			awk -F '	' '{print $2}' | cut -d '"' -f 2)
CURLpp_rank=$(echo "$CURL" |		grep -F '[0,"pp_rank"]' |			awk -F '	' '{print $2}' | cut -d '"' -f 2)
CURLpp_country_rank=$(echo "$CURL" |grep -F '[0,"pp_country_rank"]' |	awk -F '	' '{print $2}' | cut -d '"' -f 2)
CURLplaycount=$(echo "$CURL" |		grep -F '[0,"playcount"]' |			awk -F '	' '{print $2}' | cut -d '"' -f 2) # v Rounding up numbers
CURLpp_raw=$(echo "$CURL" |			grep -F '[0,"pp_raw"]' |			awk -F '	' '{print $2}' | cut -d '"' -f 2 |	awk '{print int($1+0.5)}')
CURLaccuracy=$(echo "$CURL" |		grep -F '[0,"accuracy"]' |			awk -F '	' '{print $2}' | cut -d '"' -f 2 |	awk '{print int($1+0.5)}')
CURLlevel=$(echo "$CURL" |			grep -F '[0,"level"]' |				awk -F '	' '{print $2}' | cut -d '"' -f 2 |	awk '{print int($1+0.5)}')

# ^ data, echo what you want