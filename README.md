# osu-bash-snippet

Snippets of codes that I like to share. Mainly from my bot.

## Usage

`./acc.sh [osu|mania|taiko|ctb] [50] [100] [300] [miss] [katu] [geki]`

`./lookup.sh [osu|ctb|mania|taiko] [API] [osu! username]`

## Credits

Accuracy formulas provided by ThePooN and OMKelderman.
