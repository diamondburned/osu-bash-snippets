#!/bin/bash
# Usage: ./acc.sh "osu|mania|taiko|ctb" 50 100 300 miss katu geki

score[50]="$2"
score[100]="$3"
score[300]="$4"
miss="$5"
katu="$6"
geki="$7"

if [[ $1 = "taiko" ]]; then
	gamemode="1"
	gametype="osu! Taiko"
	hitsNumber=$(( ${score[100]} + ${score[300]} + $miss ))
	hitsPoint=$(( (${score[100]} * 0.5 + ${score[300]} * 1) * 300 ))
elif [[ $1 = "ctb" ]]; then
	gamemode="2"
	gametype="osu! Catch the Beat"
	hitsNumber=$(( $miss + ${score[50]} + ${score[100]} + ${score[300]} + $katu ))
	hitsPoint=$(( ${score[50]} + ${score[100]} + ${score[300]} ))
elif [[ $1 = "mania" ]]; then
	gamemode="3"
	gametype="osu! Mania"
	hitsNumber=$(( $miss + $katu + $geki + ${score[50]} + ${score[100]} + ${score[300]} ))
	hitsPoint=$(( ${score[50]} * 50 + ${score[100]} * 100 + $katu * 200 + ${score[300]} * 300 + $geki * 300 ))
else
	gamemode="0"
	gametype="osu!"
	hitsNumber=$(( ${score[50]} + ${score[100]} + ${score[300]} + $miss ))
	hitsPoint=$(( ${score[50]} * 50 + ${score[100]} * 100 + ${score[300]} * 300 ))
fi

if [[ $gamemode = "2" ]]; then
	echo $(( $hitsPoint / $hitsNumber ))
else
	echo $(( $hitsPoint / ($hitsNumber * 3) ))
fi
